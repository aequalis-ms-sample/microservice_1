package com.aeq.microservice_1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/service_1")
public class HelloWorld {

    @GetMapping
    public String getHelloWorld() {
        return "Hello World from Service 1";
    }
}
